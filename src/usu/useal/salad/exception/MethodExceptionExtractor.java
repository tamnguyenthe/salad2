package usu.useal.salad.exception;

import java.io.IOException;
import java.util.List;

import org.jf.dexlib2.DexFileFactory;
import org.jf.dexlib2.dexbacked.*;

import com.google.common.collect.ImmutableList;
import usu.useal.salad.cfg.CFG;
import usu.useal.salad.util.Constants;

public class MethodExceptionExtractor {

    private final DexBackedMethod method;
    private final CFG cfg;
    private final List<DexBackedTryBlock> tryBlocks;

    public DexBackedMethod getMethod() {
        return method;
    }

    public CFG getCfg() {
        return cfg;
    }

    public List<DexBackedTryBlock> getTryBlocks() {
        return tryBlocks;
    }

    public MethodExceptionExtractor(DexBackedMethod method) {
        this.method = method;
        cfg = new CFG(ImmutableList.copyOf(method.getImplementation().getInstructions()));
        tryBlocks = (List<DexBackedTryBlock>) method.getImplementation().getTryBlocks();
    }

    public static void main(String[] args) throws IOException {
        DexBackedDexFile dexFile = DexFileFactory.loadDexFile("/home/tamnguyen/Documents/IdeaProjects/SALAD/Resources/examples/ReadFileEx.dex", Constants.API);
        processDexFile(dexFile);
    }

    private static void processDexFile(DexBackedDexFile dexFile) {
        for (DexBackedClassDef classDef : dexFile.getClasses()) {
            for (DexBackedMethod method : classDef.getMethods()) {
                if (method.getName().equals("readTextFile"))
                    processMethod(classDef, method);
            }
        }

    }

    private static void processMethod(DexBackedClassDef classDef, DexBackedMethod method2) {
        MethodExceptionExtractor methodExceptionExtractor = new MethodExceptionExtractor(method2);
        for (DexBackedTryBlock tryBlock : methodExceptionExtractor.getTryBlocks()) {
            System.out.println("start code address: " + tryBlock.getStartCodeAddress());
            System.out.println("code unit count: " + tryBlock.getCodeUnitCount());
            for (DexBackedExceptionHandler dexBackedExceptionHandler : tryBlock.getExceptionHandlers()) {
                System.out.println(dexBackedExceptionHandler.getExceptionType());
                System.out.println(dexBackedExceptionHandler.getHandlerCodeAddress());
            }
        }
    }


}
